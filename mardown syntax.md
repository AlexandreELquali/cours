# Markdown Syntax Resume
## Basic Syntax
##### **Element/Syntax**
Heading :
- '#'= H1 
- '##'= H2
- '###'=H3    

Bold :
`**text en gras**` 
 
Italic :`*text en italic*`    

Ordered List 
1. first item 
2. Second item   
3. Third Item

Unordered list 
- First 
- Second
- Third 

Code = Backtick avant et apres le mot   
Horizontale rule = -------    
Link = `[title](https:/www.test.com)`  
Image = `![alt text](image.jpg)`

## Extended Syntax
Footnote : `Phrase avec un footnote. [^1]`  
`[^1]: la footnote`  

Heading  ID : `### Mon Heading {random-id}`  

Task List : `- [x] Objet 1 `  
`- [ ] Objet 2 `
