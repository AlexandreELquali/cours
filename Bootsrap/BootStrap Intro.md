#Bootstrap 
## Historique 
Créé par des dev de twitter pour répondre aux besoins de portabilité à l'époque (2011)
Bootstrap intégre initialement uniquement du CSS mais depuis à ajouter des éléments html et Js   
Dans sa dernière version(3) il intègre également  l'aspect responsive, jusqu'alors présent dans un fichier séparé.
   
## C'est quoi ? Pourquoi ?
1. Bootstrap est un **Framework Css** , son code est fondé sur du **Html/CSS**
2. Les framework Css ont pour objectif premier de **standardiser** un ensemble de style homogène afin de créé des références commmun  à tous. 
## Fonctionnalité & Particularité  
Boostrap est :
* **Cross-browser** : Il s'adapte parfaitement aux différents navigateurs, ces derniers réagissant tous de la même façon
* **Responsive** : L'une des particularité principales de Bootsrap, Avec une base de 12 colonnes permettant une adaption à chaque type d'écran
* **Normalisation** : Via l'emploi de nombreux outils complémentaires : Normalize/LESS (voir fichier dédié)  

### En quelques points :
- Base de 12 colonnes ( que l'on peut changer si nécessaire)
- Utilisation de Normalize.css
- Code fondé sur Html/CSS
- Bibliothèque open source k
- Architecture basé sur LESS

  
### En un mot 
L'utilisation de Bootstrap doit permettre un gain de temps/productivité importante avec à la clef :
- Un fichier CSS réduit au maximum 
- Une simplicité pour gérer la mise en forme 
- Celle-ci est basé sur une syntax commun à grand nombre d'utilisateurs
- Et est Responsive 
