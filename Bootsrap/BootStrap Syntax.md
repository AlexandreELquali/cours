# BootStrap Syntax   

* Ce thème sera abordé et retravaillé au fur et à mesure 
## Installation de BootStrap dans un fichier Html :
Copié l'ensemble des 2 link ( sur la page Bootstrap) Css et Js puis les **introduire dans le head, juste avant le body**  
 
* Lien Css: link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">  

* Lien JS : script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script  
          script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script  
          script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script  
      *Les parties allant de 'integrity' jusqu'a 'anonymous' ne sont pas nécessaire*
      
### Architecture du fichier Html   
#### Pré-requis
* Un *div class="container"* qui englobe l'ensemble du body
* Un élement *Section* pour chaque 'contenu' de la page, intégrant un *header* et un *footer* si nécessaire
#### Les colonnes 
Chaque *Section* devra être (du moins pour débutants) englobé par par les div suivants :
* *div class="row"* 
* *div class=col-(un chiffre)*   
  
      
