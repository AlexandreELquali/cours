# Html deviens Markdown & Premier pas sur Git 
## Markdown
### C'est quoi ? 
Markdown est langage de texte permettant à l'instar d'html, de structuer le contenu textuel d'une page avec pour particularité les élements suivants :
1. Une **syntaxe simplifié à l'extrême**
2. Un **SEO** qui est de ce fait bien plus efficace à mettre en place
3. L'introduction facile de différent langage au sein d'une page avec l'emploi de triple **backtick**
4. Enfin, Markdown est **déja integré dans de nombreux editeurs de texte** présent sur l'ensemble des sites professionnels(Git,Slack) , rendant son utilisation omni-présente.   
#### En un mot
 Une atlternative  plus simple, souple, et efficace au langage Html  
 
*Remarque  : Un autre fichier est spécialement dédié à l'énumeration de la syntaxe Markdown*
## Git
### C'est quoi ? 
Git est un protocole permettant le versionnage (stockage des données numériques )de fichiers   
#### Objectifs ( liste non exhaustives)
- Facilité Travail en groupe
-  Permet de mettre en évidence des compétences pour les recruteurs 
- Sauvegarde rapide et facile sur un support en ligne de ses fichiers
- Enregistrer et retrouver aisément différentes versions antérieurs à une mise à jour

*Remarque: GitLab & GitHub sont 2 plateformes qui utilisent le protcole Git + un fichier dédier à l'explication de la connexion git/webstorm est présent*


----------
_______

                        
# Gitlab Début et Connection avec Webstorm 
## Prerequis : 
- Avoir Git déja installé sur son ordinateur ( auquel cas WebStorm le proposera lors de l'installation)
- Connaitre ses identifiant et mot de passe GitLab
### Création Premier Projet Gitlab
- Commencer par demarrer un nouveau projet sur Gitlab
- Nommé le dossier puis copier dans le bouton **clone** l'url **Https**
### Webstorm
 A l'ouverture de WebStorm il est *normalement*  possible de connecter dès le début 
- Sur Webstorm démarrer un nouveau projet en incluant l'url précedemment copié, auquel cas 
- Création de fichier avec l'extension choisi (js,md,css, etc)
- Allé dans VCS => Checkout Ffrom Version Control => Git => Copié l'url Clone https du dossier Gitlab

#### Le Fichier : Sauvegarde Local et En ligne 
2 Actions seront répétés par la suite pour actualisé les fichiers 
-  L'action **Commit** qui  Sauvegarde le fichier localement : Vcs => Git => Commit   
  **l'action peut-être réalisé par le raccours Ctrl+k**
- L'action **Push** qui actualise le fichier en ligne Gitlab : Vcx => Git =>  
**l'action peut-être réaliser par le raccourcis Ctrl+Shift+k**
 
---------------
------
# Markdown Syntax Resume
## Basic Syntax
##### **Element/Syntax**
Heading :
- '#'= H1 
- '##'= H2
- '###'=H3    

Bold :
`**text en gras**` 
 
Italic :`*text en italic*`    

Ordered List 
1. first item 
2. Second item   
3. Third Item

Unordered list 
- First 
- Second
- Third 

Code = Backtick avant et apres le mot   
Horizontale rule = -------    
Link = `[title](https:/www.test.com)`  
Image = `![alt text](image.jpg)`

## Extended Syntax
Footnote : `Phrase avec un footnote. [^1]`  
`[^1]: la footnote`  

Heading  ID : `### Mon Heading {random-id}`  

Task List : `- [x] Objet 1 `  
`- [ ] Objet 2 `

