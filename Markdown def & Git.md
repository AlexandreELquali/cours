# Html deviens Markdown & Premier pas sur Git 
## Markdown
### C'est quoi ? 
Markdown est langage de texte permettant à l'instar d'html, de structuer le contenu textuel d'une page avec pour particularité les élements suivants :
1. Une **syntaxe simplifié à l'extrême**
2. Un **SEO** qui est de ce fait bien plus efficace à mettre en place
3. L'introduction facile de différent langage au sein d'une page avec l'emploi de triple **backtick**
4. Enfin, Markdown est **déja integré dans de nombreux editeurs de texte** présent sur l'ensemble des sites professionnels(Git,Slack) , rendant son utilisation omni-présente.   
#### En un mot
 Une atlternative  plus simple, souple, et efficace au langage Html  
 
*Remarque  : Un autre fichier est spécialement dédié à l'énumeration de la syntaxe Markdown*
## Git
### C'est quoi ? 
Git est un protocole permettant le versionnage (stockage des données numériques )de fichiers   
#### Objectifs ( liste non exhaustives)
- Facilité Travail en groupe
-  Permet de mettre en évidence des compétences pour les recruteurs 
- Sauvegarde rapide et facile sur un support en ligne de ses fichiers
- Enregistrer et retrouver aisément différentes versions antérieurs à une mise à jour

*Remarque: GitLab & GitHub sont 2 plateformes qui utilisent le protcole Git + un fichier dédier à l'explication de la connexion git/webstorm est présent*




                        