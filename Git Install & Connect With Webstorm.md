# Gitlab Début et Connection avec Webstorm 
## Prerequis : 
- Avoir Git déja installé sur son ordinateur ( auquel cas WebStorm le proposera lors de l'installation)
- Connaitre ses identifiant et mot de passe GitLab
### Création Premier Projet Gitlab
- Commencer par demarrer un nouveau projet sur Gitlab
- Nommé le dossier puis copier dans le bouton **clone** l'url **Https**
### Webstorm
 A l'ouverture de WebStorm il est *normalement*  possible de connecter dès le début 
- Sur Webstorm démarrer un nouveau projet en incluant l'url précedemment copié, auquel cas 
- Création de fichier avec l'extension choisi (js,md,css, etc)
- Allé dans VCS => Checkout Ffrom Version Control => Git => Copié l'url Clone https du dossier Gitlab

#### Le Fichier : Sauvegarde Local et En ligne 
2 Actions seront répétés par la suite pour actualisé les fichiers 
-  L'action **Commit** qui  Sauvegarde le fichier localement : Vcs => Git => Commit   
  **l'action peut-être réalisé par le raccours Ctrl+k**
- L'action **Push** qui actualise le fichier en ligne Gitlab : Vcx => Git =>  
**l'action peut-être réaliser par le raccourcis Ctrl+Shift+k**
 
